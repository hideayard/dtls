<table style="width: 97%; text-align: left; margin-left: auto; margin-right: 0px;" border="0" cellpadding="0" cellspacing="0">

    <tbody>
  
      <tr>
  
        <td style="text-align: left; width: 726px;">
        <p class="MsoNormal" style="line-height: normal; font-family: Verdana;"><b><span style="font-size: 24pt;">ALAT-ALAT
  KAWALAN SISTEM WIRING PENDAWAIAN ELEKTRIK<o:p></o:p></span></b></p>
  
        <p class="MsoNormal" style="line-height: normal;"><b style="font-family: Verdana;"><span style="font-size: 12pt;">ALAT-ALAT
  KAWALAN PENDAWAIAN ELEKTRIK SATU FASA</span></b><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Definisi:-<o:p></o:p></span></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Litar kawalan
  ialah turutan alat-alat kawalan &amp; perlindungan yang terdapat
  pada litar utama pemasangan pengguna.Litar ini termasuklah pengasingan,
  pensuisan, perlindungan arus lebihan dan perlindungan kebocoran arus ke
  bumi. Alat-alat
  kawalan dan perlindungan dalam litar kawalan adalah seperti berikut:-<o:p></o:p></span></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 595px; height: 107px;" alt="" src="dist/img/step2/litar kawalan wiring elektrik.png"></p>
  
        <ol>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Unit
  pemotong (Cut out) <o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Jangka
  kilowattjam ( Kilowatt-hourmeter) <o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Suis
  utama (Main Switch) <o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Pemutus
  litar bocor ke bumi (<i>Earth Leakage Circuit Breaker</i>) <o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Kotak
  fius agihan ( <i>Distribution fuse board</i> )<o:p></o:p></span></li>
  
        </ol>
  
        <p class="MsoNormal" style="line-height: normal;"><b><i style=""><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">FIUS
  PERKHIDMATAN DAN PENGHUBUNG NEUTRAL</span></i></b><i style=""><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><u1:p></u1:p><o:p></o:p></span><img style="width: 554px; height: 211px;" alt="" src="dist/img/step2/fius perkhidmatan pendawaian elektrik.png"></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Fius ini di
  sambung pada punca masukan bekalan utama iaitu ke pengalir hidup
  atau fasa. Berfungsi Sebagai&nbsp; lebihan dan menghadkan arus
  pengguna dan
  penyambung neutral digunakan untuk menyambung kabel bekalan utama
  kepada kabel
  neutral pengguna.<o:p></o:p></span></p>
  
        <p class="MsoNormal" style="line-height: normal;"><b><i style=""><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">METER
  KILOWATT/JAM</span></i></b><i style=""><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 210px; height: 248px;" alt="" src="dist/img/step2/meter kilowatt jam pendawaian elektrik.png"></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Berfungsi
  sebagai pengukur jumlah tenaga yang digunakan oleh pengguna
  bagi dalam satu tempoh masa yang tertentu bacaan yang direkodkan dan
  jumlah
  penggunaan akan di kira untuk bayaran bagi setiap unit.<o:p></o:p></span></p>
  
        <p class="MsoNormal" style="line-height: normal;"><b><i style=""><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">SUIS
  UTAMA</span></i></b><i style=""><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 223px; height: 262px;" alt="" src="dist/img/step2/suis utama wiring elektrik 1 phase.png"></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Suis utama
  berfungsi sebagai pengasing litar dan memberi perlindungan
  arus berlebihan, alat kawalan secara mekanikal yang boleh membuka dan
  menyambung litar secara kawalan manual.<o:p></o:p></span></p>
  
        <p class="MsoNormal" style="line-height: normal;"><b><i style=""><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">PEMUTUS
  LITAR BOCOR</span></i></b><i style=""><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><u1:p></u1:p><o:p></o:p></span><img style="width: 222px; height: 248px;" alt="" src="dist/img/step2/pemutus litar bocor elcb rcd rccb.png"></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Berfungsi
  sebagai perlindungan terhadap&nbsp; kebocoran arus ke bumi dan
  pengasing.<o:p></o:p></span></p>
  
        <p class="MsoNormal" style="line-height: normal;"><b><i style=""><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">KOTAK
  AGIHAN DB BOX</span></i></b><i style=""><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-size: 12pt; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 233px; height: 232px;" alt="" src="dist/img/step2/kotak agihan db box pendawaian elektrik.png"></p>
  
        <p class="MsoNormal" style="line-height: normal;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Kotak agihan DB
  ini adalah tempat pengagihan litar kecil akhir dan
  peranti perlindungan arus lebihan.setiap litar kecil akhir mempunyai
  peranti
  perlindungan arus lebihan yang tersendiri mengikut kadaran arus maksima
  dan jenis
  litarnya&nbsp; Berfungsi sebagai pengagih litar akhir dan
  perlindungan arus
  lebihan.<o:p></o:p></span></p>
  
        <p class="MsoNormal"><o:p></o:p>
        <script async="" src="dist/img/step2/f.txt"></script><!-- leaderboard -->
        <script>
  (adsbygoogle = window.adsbygoogle || []).push({});
        </script></p>
  
        <h1><span style="font-size: 24pt; line-height: 115%;"><span style="font-family: Verdana;">WIRING
  ELEKTRIK PENDAWAIAN
  LITAR PENGGUNA</span><o:p></o:p></span></h1>
  
        <p class="MsoNormal" style=""><b><span style="font-size: 12pt; line-height: 115%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">LITAR
  AKHIR PENGGUNA <span style="font-style: italic;">WIRING
  ELEKTRIK</span></span></b><span style="font-size: 12pt; line-height: 115%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Litar akhir
  wiring elektrik adalah
  litar yang disambungkan keluar dari kotak fius agihan (DB) dan berakhir
  di
  beban atau soket alur keluar atau kelengkapan elektrik di pemasangan
  itu. (Mula
  dari DB dan berakhir ke beban pengguna).<o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">&nbsp;Terdapat
  2 jenis litar
  akhir&nbsp; iaitu:-<o:p></o:p></span></p>
  
        <p class="MsoListParagraphCxSpFirst" style="margin-left: 39pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><span style="">1.<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;
        </span></span></span><!--[endif]--><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Litar
  pencahayaan - litar lampu,
  kipas,loceng dan jam elektrik dll.<o:p></o:p></span></p>
  
        <p class="MsoListParagraphCxSpLast" style="margin-left: 39pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><span style="">2.<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;
        </span></span></span><!--[endif]--><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Litar kuasa -
  soket alur keluar(sak)
  biasanya 13A, 15A.<o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><b><i style=""><span style="font-size: 12pt; line-height: 115%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">LITAR
  PENCAHAYAAN</span></i></b><i style=""><span style="font-size: 12pt; line-height: 115%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Litar ini
  termasuk untuk lampu,
  kipas siling, loceng pintu dan jam elektrik. jumlah beban yang boleh
  dipasang
  pada satu litar kecil akhir lampu adalah berkadaran 5A atau 6A. Setiap
  satu
  point lampu atau kipas dianggarkan berkadaran tidak lebih 100W. Satu
  litar
  akhir bagi litar lampu/kipas jumlah unit maksima ialah 10 unit lampu
  dan kipas.<o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Contoh cara
  pendawaian litar
  cahaya:-<o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><u1:p></u1:p><o:p></o:p></span><img style="width: 432px; height: 304px;" alt="" src="dist/img/step2/wiring litar lampu.png"></p>
  
        <p class="MsoNormal" style=""><b><i style=""><span style="font-size: 12pt; line-height: 115%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">LITAR
  KUASA (SOKET ALUR KELUAR)</span></i></b><i style=""><span style="font-size: 12pt; line-height: 115%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Litar Kuasa
  dalam wiring elektrik
  terbahagi kepada dua jenis:- <o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">1. Litar Jejari<o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">&nbsp;Litar
  ini adalah sejenis litar
  pendawaian biasa di mana dawai hidup-(l), dawai neutral-(n) dan dawai
  bumi-(e)
  diambil dari kotak fius agihan sebagai satu litar akhir dan
  disambungkan ke
  punca soket alur keluar (S.A.K).<o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">2. Litar Gelang
  (ring)<o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Kabel litar
  gelang mestilah bermula
  dari satu fius di dalam kotak fius agihan dan disambungkan ke tiap-tiap
  punca
  soket alir keluar (S.A.K) dan akhirnya hendaklah kembali ke fius
  asalnya.<o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Contoh
  pendawaian elektrik litar
  kuasa&nbsp;:-<o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 449px; height: 281px;" alt="" src="dist/img/step2/pendawaian elektrik litar soket.png"></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">Contoh
  pendawaian soket litar jejari &amp;
  gelang:-<o:p></o:p></span></p>
  
        <p class="MsoNormal" style=""><span style="font-family: &quot;Cambria&quot;,&quot;serif&quot;;"><img style="width: 461px; height: 284px;" alt="" src="dist/img/step2/wiring elektrik litar gelang litar jejari.png"><br>
  
        </span></p>
  
        <h3><span style="font-size: 26pt; line-height: 115%;"><span style="font-family: Verdana;">PENDAWAIAN
  ELEKTRIK 1 Phase &amp;
  3 Phase Rumah Bangunan</span><o:p></o:p></span></h3>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><o:p></o:p></span></b>
        <script>
  (adsbygoogle = window.adsbygoogle || []).push({
  google_ad_client: "ca-pub-3694619358399549",
  enable_page_level_ads: true
  });
        </script></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">PENGENALAN</span></b><b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">
        </span></b><b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">SISTEM
  PENDAWAIAN WIRING ELEKTRIK</span></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  pendawaian elektrik terbahagi kepada 2 bahagian iaitu:</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <ul>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  pendawaian 1 Phase / fasa – biasa di kawasan kediaman
  (residential area) kecil dan sederhana.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  pendawaian 3 Phase / fasa – kawasan industri dan
  komersial (industrial dan commercial area).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
        </ul>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><br>
  
        <i>BEKALAN ELEKTRIK ke BANGUNAN KEDIAMAN</i></span></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Bekalan
  elektrik yang diterima di rumah kediaman
  (kecil/sederhana) biasanya ialah AC 1-fasa 240V, 50 Hz.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Bekalan
  ini
  diperolehi daripada sambungan antara talian (sama ada R, Y atau B) dan
  neutral
  dari sistem 3 fasa 4 dawai (sambungan Y).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sumber
  3 fasa adalah dari pencawang (substation) yang
  berhampiran.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 319px; height: 243px;" alt="" src="dist/img/step2/sistem bekalan pengguna wiring elektrik.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  bekalan pengguna kecil arus
  ulang-alik (1fasa)</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  Pengguna</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kabel
  atau talian yang masuk ke bangunan terus disambung ke satu
  papan penyambung (pangkalan perkhidmatan).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Di
  papan ini terdapat fius perkhidmatan dan talian
  neutral. Dua alat ini milik TNB dan menjadi tanggungjawabnya.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Selepas
  pangkalan perkhidmatan, bekalan elektrik akan disambung ke meter
  kilowatt jam
  (Watt hour meter).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  pengguna bermula dengan papan suis utama (main switch
  board (msb)) atau pengasing berkadaran 40A, 60A, 80A dan 100A.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Seterusnya
  ialah pemutus litar ke bumi (ELCB – <i>earth leakage
  circuit breaker</i> atau
  RCCB - <i>residual current circuit breaker</i>).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Diikuti
  dengan papan fius agihan atau
  papan agihan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Di
  papan fius agihan ini terdapat 3 busbar penyambungan iaitu
  wayar hidup (live), neutral dan bumi (earth).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Fius–fius
  (pemutus litar miniatur @
  miniature circuit breaker (MCB)) yang berkadaran sama ada 5A, 15A, 30A,
  45A
  atau 6A, 16A dan 32A ini akan disambung daripada busbar live ke litar
  akhir
  yang bersesuaian.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Bar
  neutral dan busbar bumi akan disambung terus pada litar
  akhir – tidak perlu melalui MCB lagi.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  akhir akan mempunyai 3 dawai iaitu live, neutral dan
  bumi.</span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><img style="width: 186px; height: 404px;" alt="" src="dist/img/step2/sistem bekalan elektrik rumah 1 phase.png"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  Bekalan Elektrik Di Rumah (240
  V, 1- Fasa, 50 Hz)</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  Akhir</span></i></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><br>
  
  Litar yang keluar daripada papan fius agihan bagi membekalkan satu atau
  lebih
  titik-titik kelengkapan atau soket alur keluar tanpa perantaraan papan
  agihan
  lain atau dengan kata lain litar yang bersedia membekalkan tenaga
  elektrik
  kepada peralatan, kelengkapan atau soket alir keluar seperti lampu,
  kipas atau
  melalui soket alit keluar seperti sterika, TV, peti sejuk dll.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <img style="width: 293px; height: 326px;" alt="" src="dist/img/step2/pemencilan agihan pendawaian elektrik.png">
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemencilan
  secara elektrik</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Umumnya,
  litar akhir untuk bangunan kediaman boleh dibahagikan
  kepada kumpulan berikut:</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 3pt 54pt; background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-indent: -18pt; line-height: 150%;"><!--[if !supportLists]--><span style="font-family: Symbol;"><span style="">·<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </span></span></span><!--[endif]--><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  akhir pada kadaran arus kurang
  daripada 15A.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 3pt 54pt; background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-indent: -18pt; line-height: 150%;"><!--[if !supportLists]--><span style="font-family: Symbol;"><span style="">·<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </span></span></span><!--[endif]--><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  akhir pada kadaran arus lebih
  daripada 15A.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoListParagraphCxSpLast" style="margin: 0cm 0cm 3pt 54pt; background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-indent: -18pt; line-height: 150%;"><!--[if !supportLists]--><span style="font-family: Symbol;"><span style="">·<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </span></span></span><!--[endif]--><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  akhir pada kadaran arus lebih
  daripada 15A tetapi tertumpu pada soket alir keluar 13A sahaja.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><b><i style=""><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Peraturan
  Litar Akhir</span></i></b><i style=""><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  akhir hendaklah diasingkan untuk peralatan yang berbeza
  (berlainan fius dan kawalannya) bagi mencegah gangguan bekalan jika
  litar akhir
  lain mengalami kerosakan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sebagai
  contoh, antara lampu dan soket alir keluar
  hendaklah diasingkan fiusnya.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pendawaian
  setiap litar akhir hendaklah diasingkan secara
  elektrik (neutral dan live berlainan) kecuali ke bumi.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 321px; height: 326px;" alt="" src="dist/img/step2/contoh skematik litar akhir pendawaian elektrik rumah.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Contoh
  gambar rajah skematik bagi pelbagai litar akhir</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  Akhir untuk Soket Alir Keluar</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Terdapat
  2 cara pendawaian soket alir keluar (2, 5, 13, 15 dan
  30 A) iaitu Litar Gelang (ring circuit) dan Litar Jejari (radial
  circuit).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <ol>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sambungan
  litar gelang – sambungan soket alir keluar yang
  bersambung antara satu sama lain dalam bentuk gelang.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sambungan
  bermula dan berakhir pada tempat yang sama iaitu di papan fius agihan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sambungan
  litar jejari – soket alir keluar disambung secara
  jejari atau selari.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
        </ol>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Saiz
  kabel yang biasa digunakan untuk litar gelang ialah 2.5 mm2
  dan 4 mm2 bagi litar jejari.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 307px; height: 326px;" alt="" src="dist/img/step2/litar jejari wiring elektrik.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  jejari (radial circuit)</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 191px;" alt="" src="dist/img/step2/litar gelang pendawaian elektrik.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  gelang (ring circuit)</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Penentuan
  Saiz Kabel dan Peranti
  Perlindungan</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kadar
  fius atau pemutus litar miniatur yang mengawal sesuatu
  litar akhir hendaklah tidak melebihi kadar kemampuan membawa arus
  mana–mana
  kabel dalam litar tersebut.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Besar
  kecilnya kabel dan fius sesuatu litar adalah
  bergantung pada jenis beban yang hendak digunakan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Umumnya,
  bagi
  rumah kediaman, saiz kabel yang digunakan ialah 1.5mm, 2.5mm dan 4mm<sup>2</sup>.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Saiz
  fius yang
  biasa digunakan pula ialah 5A, 15A dan 30A.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><span style="font-size: 14pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">JENIS
  – JENIS PENDAWAIAN</span></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><br>
  
  Terdapat 3 jenis pendawaian elektrik yang biasa dilakukan iaitu:</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <ul>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pendawaian
  Permukaan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pendawaian
  Terbenam.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pendawaian
  Pembuluh (conduit) - (i) Pembuluh keluli, bukan
  keluli dan boleh lentur, (ii) Sesalur, (iii) Salur</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
        </ul>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Faktor
  yang perlu diambil kira dalam pemilihan jenis pendawaian
  adalah:</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <ul>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Jenis
  beban yang hendak dipasang</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Keadaan
  tempat pemasangan</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kos
  perbelanjaan</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Tempoh
  ketahanan pemasangan</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kekemasan
  dan kecantikan</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Keadaan
  sekeliling</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Tempoh
  masa menyiapkan</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Perubahan
  atau penambahan litar di masa akan datang</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Jenis
  voltan bekalan</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Keselamatan
  dan kelulusan ST/TNB/JKR</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
        </ul>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pendawaian
  Permukaan (Surface Wiring)</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Satu
  sistem di mana kabel – kabel yang digunakan dalam sesuatu
  pemasangan dipasang pada permukaan dinding atau siling tanpa sebarang
  perlindungan tambahan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kabel
  yang digunakan mestilah daripada jenis bersalut kerana
  terdedah kepada keadaan persekitaran dan kerosakan mekanikal.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 298px;" alt="" src="dist/img/step2/pendawaian permukaan.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pendawaian
  yang bertebat di permukaan</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Bagi
  pendawaian permukaan yang dibuat di bangunan batu satu
  simen, kabel akan diklipkan atas satu bilah papan (wooden batten) yang
  sebelum
  ini dipakukan ke dinding atau siling.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Jenis
  klip yang biasa digunakan ialah klip aluminium atau
  plumbum.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kebaikan
  sistem ini ialah ia mudah disiapkan dengan perbelanjaan
  yang murah, pemasangan tambahan juga mudah dilakukan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Biasanya
  diaplikasikan di rumah kediaman (lama dan sudah tidak digunakan di
  kawasan
  perumahan baru untuk tujuan keselamatan).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kelemahan
  sistem ini ialah ia tidak tahan dengan suhu yang
  panas dan lembap, tidak cantik dan sistem perlindungan mekanik yang
  kurang
  memuaskan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pendawaian
  Terbenam (Embedded Wiring)</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kabel
  litar dipasang di dalam dinding atau siling dan tidak
  kelihatan langsung kecuali penghujung kabel yang digunakan untuk
  sambungan ke
  terminal aksesori atau beban.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kaedah
  ini dilakukan dengan membenamkan kabel ke lurah
  alur di dinding atau siling yang telah disediakan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pada
  kebiasaannya ia dibuat semasa peringkat pembinaan bangunan iaitu
  sebelum kerja
  penurapan simen (plaster) dijalankan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kabel
  tersebut mestilah disusun dengan rapi agar tidak
  berlaku sebarang kerosakan mekanikal ke atasnya.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kaedah
  ini cantik dan kemas. Ia tidak
  menghadapi bahaya mekanikal dn tahan pada perubahan cuaca, tahan lama
  dan
  kosnya juga rendah.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Tetapi
  sistem ini sukar untuk dibaiki apabila berlaku kerosakan
  dan boleh mendatangkan bahaya semasa memaku dinding yang tersimpan
  kabel yang
  tersembunyi.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pendawaian
  Pembuluh (conduit)</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Dalam
  sistem ini, konduit digunakan untuk menyalurkan
  kabel–kabel.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  ini digunakan di tempat yang terdapat bahaya kerosakan
  mekanik atau kimia, contohnya di kilang–kilang perusahaan,
  makmal, bengkel dll.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 210px; height: 324px;" alt="" src="dist/img/step2/pendawaian konduit.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Penyesuai
  dan pembuluh boleh lentur</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  ini boleh dipasang dipermukaan atau tersembunyi.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Beberapa
  keistimewaan pendawaian sistem ini ialah konduit ini boleh dijadikan
  pengalir
  perlindung litar (grounding), mudah diubahsuai untuk tambahan, tahan
  lama,
  dapat mengelakkan kebakaran dan kabelnya tidak mudah tercacat.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  ini
  melibatkan perbelanjaan yang tinggi serta mengambil masa yang lama
  untuk
  disiapkan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Bentuknya
  tidak kemas jika dipasang pada permukaan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Untuk
  pembuluh
  boleh lentur (logam) yang boleh dibentuk mengikut kehendak pemasangan
  amat
  berguna terutamanya untuk sambungan bagi penghujung beban yang bergerak</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">atau
  di tempat
  – tempat yang bebannya selalu diubahsuai.<o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><b style=""><i style=""><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pendawaian
  Sesalur</span></i></b><i style=""><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  pendawaian ini menggunakan sesalurlogam atau bahan
  penebat yang pada kebiasaanya bersegi empat dan dipasang secara menegak
  atau
  mendatar di permukaan dinding atau besi rangka bangunan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kabel
  dimuatkan dalam sesalur dan biasanya bersaiz besar serta bilangan yang
  banyak.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 289px;" alt="" src="dist/img/step2/palang bas sesalur.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Palang
  bas sesalur atas</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 278px;" alt="" src="dist/img/step2/sistem pendawaian sesalur.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  pendawaian palang bas sesalur
  atas</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><b style=""><i style=""><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pendawaian
  Salur</span></i></b><i style=""><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Dibuat
  daripada logam yang berbentuk segi empat bujur memanjang
  tanpa penutup.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Lazimnya,
  salur ini tersembunyi di bawah lantai yang biasa
  disebut salur bawah lantai.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Jika
  dilihat dari atas, sistem ini dipasang seperti sistem
  grid dalam sesebuah bangunan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kesemua
  kotak simpang berada pada selekoh bengkokan 90.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  ini
  amat sesuai digunakan di pejabat kerana alatan dan perkakas sering
  diubah-ubah
  kedudukannya.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 284px;" alt="" src="dist/img/step2/sistem pendawaian salur.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  pendawaian salur bawah tanah</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><span style="font-size: 26pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">KABEL/WAYER</span></b><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kabel
  ialah bahan pengalir yang membenarkan arus mengalir.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Ia
  merupakan
  media yang amat penting dalam pemasangan elektrik.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Ia
  menjadi
  perantaraan sambungan bagi membekalkan tenaga elektrik kepada peralatan
  elektrik.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Oleh
  sebab kadaran arus, voltan, keadaan alat dan tempat yang
  berbeza – beza, kabel juga dibuat berbeza-beza bagi
  menyesuaikannya dengan
  keperluan aplikasi tersebut.</span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;">
        <script>
  (adsbygoogle = window.adsbygoogle || []).push({
  google_ad_client: "ca-pub-3694619358399549",
  enable_page_level_ads: true
  });
        </script><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"></span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Faktor
  Pemilihan Kabel</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Saiz
  kabel mampu membawa arus yang diperlukan oleh sesuatu beban
  tanpa memanaskan kabel.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Susut
  voltan tidak melebihi 2.5 % voltan bekalan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Penebat
  kabel
  sesuai dengan keadaan (kabel PVK sesuaiuntuk suhu 0 - 65? C).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Bahagian–bahagian
  kabel dan Istilah
  Kabel</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sekurang
  kurangnya kabel mempunyai bahagian yangdikenali sebagai
  Pengalir, Penebat dan Perlindungan Mekanik.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Teras
  (core) – pengalir bertebat, cth
  1, 2, 3, 4 dll.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Lembar
  (strand) – bilangan pengalir pada sesuatu teras biasanya
  3, 7, 19, 24, 37, 91 dll. Contoh kabel 7/1.04 mm (7 lembar dan garis
  pusat
  setiap lembar ialah 1.04 mm).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><b style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sarung
  kabel</span></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">
  - (jacket).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><b style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Luas
  keratan rentas pengalir</span></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">
  – jumlah luas keratan rentas setiap lembar pengalir. Contoh
  kabel 7/0.85 mm ialah kabel saiz 4mm2.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><b style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Penebat</span></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">
  – bahan penebat yang mengelilingi pengalir untuk mencegah
  arus
  daripada terbocor. Contoh penebat getah, polivinil klorida (PVK),
  kertas dll.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><b style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pengalir</span></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">
  – bahan yang selalu digunakan ialah kuprum, aluminium, perak
  dll.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kadaran
  voltan – kadar ketahanan kabel dari segi penebatnya
  apabila sesuatu voltan dikenakan. Contoh 600/1000 V bermakna 600 V
  adalah
  ketahanan voltan antara pengalir dengan bumi manakala 1000 V adalah
  ketahanan
  voltan antara pengalir dengan pengalir.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><b style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kadaran
  arus</span></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">
  – kadaran arus maksimum yang mampu dibawa oleh kabel tanpa
  memanaskan pengalir atau merosakkan penebatnya.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 179px;" alt="" src="dist/img/step2/keratan rentas kabel elektrik.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Keratan
  rentas satu kabel yang
  menunjukkan pengalir, penebat dan perlindungan mekanik pada kabel
  bertebat pvc
  berperisai wayar keluli</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Warna
  Teras Kabel (tidak boleh lentur) Penebat PVC dan Getah.
  Berikut Fungsi Kabel Pengenalan Warna:-</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Bumi
  / Pengalir perlindungan - Hijau dan kuning @ hijau</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pengalir
  Live 1 fasa - Merah @ Kuning @ biru<o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pengalir
  Neutral 1 fasa - Hitam</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Fasa
  L1 bagi litar 3 fasa - Merah</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Fasa
  L2 bagi litar 3 fasa - Kuning</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Fasa
  L3 bagi litar 3 fasa - Biru</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Neutral
  bagi litar AC 1 fasa dan 3 fasa Hitam</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><span style="font-size: 26pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">PERLINDUNGAN</span></b><span style="font-size: 26pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Perlindungan
  bermakna memberi jagaan kepada alat–alat pemasangan
  daripada sebarang bahaya yang disebabkan oleh arus elektrik seperti
  arus lebih,
  kebocoran arus ke bumi, litar pintas, kilat dan sebagainya.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Di
  samping
  perlindungan daripada bahaya kilat, 3 jenis perlindungan yang perlu
  diadakan:
  (Perlindungan untuk mengawal pemasangan atau Pemencil, Perlindungan
  arus lebih
  (Fius, Pemutus Litar, Geganti), Perlindungan daripada renjatan
  elektrik).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemencil
  (Isolator)</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Alat
  yang boleh memutuskan bekalan pengagihan kepada
  pengguna dengan membukakan sambungan kabel hidup dan neutral secara
  serentak.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Untuk
  bekalan 1 fasa – ia menggunakan suis 2 kutub berangkai.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Untuk
  bekalan 3 fasa – ia menggunakan suis 3 kutub berangkai.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Tujuan
  mengadakan pemencil – mengasingkan litar dan seterusnya
  dapat melindungi pengguna daripada renjatan elektrik yang berterusan
  jika
  berlaku kebocoran dan sentuhan renjatan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Biasanya
  pemencil dipasang pada permulaan litar.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Selain
  itu,
  pemencil boleh digunakan untuk memutuskan bekalan sebelum kerja-kerja
  membaiki
  kerosakan atau membuat pendawaian tambahan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kadaran
  pemencil hendaklah dipadankan
  dengan kadaran arus bekalan dan beban.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Yang
  penting, pemencil mestilah mampu membawa arus beban
  penuh dan berkeupayaan memutuskan dan menyambungkan bekalan seperti
  yang
  diperlukan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Perlindungan
  Arus Lebih (Over Current
  Protection)</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Alat
  ini boleh memutuskan litar bekalan dengan sendiri
  (automatik) apabila berlaku arus lebih (litar pintas dll) daripada yang
  dihadkan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Alat
  ini dipasang pada dawai hidup di awal litar iaitu secara
  bersiri dengan litar bekalan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Tujuan
  memasang pelindung arus lebih pada sesuatu
  pemasangan ialah untuk mengelakkan berlakunya kerosakan pada sesuatu
  alat
  apabila arus yang melebihi kadaran keupayaan maksimum alat itu mengalir
  di
  dalam litar.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Disamping
  itu, ia juga melindungi kabel daripada rosak.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Umumnya,
  pelindung arus lebih terbahagi kepada 3 iaitu: (Fius, Pemutus litar
  (circuit
  breaker), Geganti (relay))</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">JENIS
  PELINDUNG ARUS LEBIH</span></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><br>
  
        </span><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Fius</span></i></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><br>
  
  Asasnya ialah dawai pengalir yang akan lebur apabila arus yang mengalir
  melebihi had keupayaannya. Terdapat 4 jenis fius: (Fius yang boleh
  didawai
  semula, Fius katrij, Fius keupayaan pemutus tinggi dan Fius palam</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 281px;" alt="" src="dist/img/step2/fius separuh tertutup.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Fius
  separuh tertutup dan fius
  terdedah</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 160px;" alt="" src="dist/img/step2/fius katrij.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Fius
  katrij bentuk keranda dan bentuk
  bilah pisau</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar (Circuit Breaker)</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Dapat
  dikendali secara automatik dan boleh dilaraskan pada nilai
  (arus dan masa) yang dikehendaki.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Tidak
  perlu ditukar ganti, boleh dihidupkan semula.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 254px;" alt="" src="dist/img/step2/pemutus litar maniatur.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar jenis miniatur MCB</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Terdapat
  beberapa cara pemutus litar dilakukan: (a. Cara
  terma, b. Cara magneto hidraulik, c. Cara elektromagnet)</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Geganti
  (Relay)</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Memutuskan
  litar secara tidak langsung.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Litar
  geganti
  biasanya dibuat secara kendalian terma atau magnet untuk
  disalingkuncikan
  dengan bekalan utama melalui sesentuh.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Banyak
  digunakan pada litar pemula motor.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Tidak
  perlu
  ditukar ganti, boleh direset semula.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Geganti
  boleh dilaraskan ke beberapa kadaran arus yang
  diperlukan dan boleh dilaraskan (reset) semula apabila sambungan telah
  terputus.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  Litar Cara Terma</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Komponen
  yang terlibat dalam kendalian ini terdiri daripada
  lapisan dwilogam yang dicantumkan kepada satu sesentuh sebagai satu
  tempat
  sambungan suis.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Jika
  arus yang masuk ke litar itu tidak melebihi jumlah yang
  dihadkan, dwilogam itu tidak akan memberi apa-apa kesan (sesentuh masih
  membuat
  sambungan seperti biasa).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Tetapi
  apabila arus itu melebihi arus yang dihadkan masuk
  ke dalam litar, dwilogam akan menjadi panas dan seterusnya meleding
  (kerana
  kadar pengambangannya berbeza).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Apabila
  keadaan ini terjadi, litar akan terbuka kerana
  tarikan spring.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kadaran
  arus pemutus litar ini boleh dilaraskan dengan skru
  pelaras, jadi ia akan membuka sambungan dengan cepat atau sebaliknya
  mengikut
  pelarasan skru itu.</span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><img style="width: 324px; height: 132px;" alt="" src="dist/img/step2/pemutus litar jenis maniatur.png"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"></span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar jenis miniatur dengan
  kendalian cara terma</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  Litar Cara Magneto Hidraulik</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Komponen
  yang terlibat ialah gegelung magnet yang digulung pada
  satu despot dan di tengah–tengahnya dipasang omboh secara
  terus atau rangkaian
  ke suis sesentuh.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kendaliannya
  bergantung pada kekuatan medan magnet dan kekuatan
  magnet itu pula bergantung pada kadaran arus yang mengalir padanya.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Gegelung
  ini
  dibuat dalam beberapa kadaran arus yang diperlukan dan gegelung ini
  menjadi
  penentu kadaran pelindung arus lebih yang dikehendaki.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Gegelung
  magneto-hidraulik akan menolak omboh apabila kadaran arus yang mengalir
  dalam
  litar itu melebihi hadnya dan seterusnya akan membuka sesentuh.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 303px; height: 324px;" alt="" src="dist/img/step2/pemutus litar maniatur magnet.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar jenis miniatur dengan
  kendalian cara magnetohidraulik</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  Litar Cara Elektromagnet</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Satu
  atau lebih sesentuh mungkin dikendalikan oleh gegelung
  elektromagnet.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kadar
  pelarasan kawalan pelindung arus lebih dibuat dengan skru
  pelaras atau dengan menambah bilangan belitan gegelung untuk perubahan
  besar.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">
        </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Setelah
  pemutus litar berkendali, ia boleh dikembalikan ke kedudukan asal
  dengan
  menggunakan punat tekan set semula.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Apabila
  arus yang masuk ke litar itu tidak melebihi
  kadaran yang dihadkan oleh pemutus litar, gegelung elektromagnet tidak
  akan
  mempunyai tenaga yang cukup untuk menarik sesentuh dan seterusnya
  memutuskan
  litar kerana kekuatan sesentuh lebih besar daripada kekuatan gegelung
  elektromagnet.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Tetapi
  sebaliknya, sebaik sahaja arus yang memasuki gegelung
  elektromagnet itu melebihi hadnya, gegelung magnet itu akan menarik
  sesentuh
  dan seterusnya litar itu akan terputus (buka @ off).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 127px;" alt="" src="dist/img/step2/mcb elektro magnet.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar jenis miniatur dengan
  kendalian cara elektromagnet</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  Litar Cara Geganti</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Apabila
  punat tekan hidup ditekan, arus akan masuk mengikut arah
  anak panah dan seterusnya melengkapkan litar gegelung C.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Gegelung
  C
  akan memegang semua sesentuh S1, S2 dan S3 ke kedudukan bersentuh (on).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Ini
  membolehkan arus mengalir ke motor (beban), arah anak panah.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Jika
  motor ini
  dipasang pada beban yang tidak melebihi beban penuhnya, arus akan
  mengalir
  dengan tidak memberi apa–apa kesan kepada pemanas P.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Tetapi
  jika
  arus yang masuk itu lebih besar daripada yang dihadkanoleh geganti
  akibat
  penambahan beban motor, maka arus yang lebih besar akan mengalir dan
  akan
  memanaskan P.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Seterusnya
  suis beban (suis beban lampau) akan ditolak dan
  terbuka (off).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Apabila
  ini terjadi, motor akan berhenti sebab tiada arus yang
  mengalir.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Oleh
  itu suis S1, S2 dan S3 akan terbuka (off).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 224px;" alt="" src="dist/img/step2/geganti.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Geganti
  atau sesentuh sebagai
  pelindung arus lebih</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><span style="font-size: 26pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Perlindungan
  dari Renjatan Elektrik</span></b><span style="font-size: 26pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Peralatan,
  kelengkapan dan pemasangan elektrik mestilah
  dilindungi dari arus bocor atau disentuh oleh manusia dan haiwan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sentuhan
  ini
  mengakibatkan renjatan elektrik.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kecil
  atau besarnya sesuatu renjatan bergantung pada
  rintangan badan, voltan dan arus bekalan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Oleh
  itu, pengalir mestilah ditebat dan tiada mana–mana
  bahagian litar terdedah kepada sentuhan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Perkara
  ini akan dibincangkan lebih lanjut di bahagian
  pembumian.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">PEMBUMIAN</span></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Merupakan
  salah satu kaedah perlindungan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Ia
  juga
  merupakan sambungan yang dibuat antara logam dan bumi.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Bumi
  merupakan
  pengalir yang terbesar dari segi saiz dan jumlah kawasan liputan
  menyediakan
  laluan bergalangan rendah bagi arus rosak atau arus bocor.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sebarang
  benda
  yang disambung akan mempunyai keupayaan sifar.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Bumi
  yang berada pada beza-upaya
  sifar (Beza-upaya rujukan) dapat menyahkan beza-upaya yang besar dengan
  cepat.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">
        </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Inilah
  tujuan
  asas pembumian sesuatu alat atau benda iaitu sebagai laluan balik arus
  atau
  untuk keselamatan.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Oleh
  itu, pembumian memberi keselamatan daripada bahaya renjatan
  elektrik dan kebakaran.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Apa
  Yang Perlu Dibumikan (Disambung
  ke Bumi)</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Semua
  struktur logam dalam sistem pendawaian (yang bukan membawa
  arus) seperti saluran logam, perisai pembuluh, salur, sesalur, dawai
  katenari
  dan sebagainya.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Struktur
  logam terdedah bagi suatu peralatan elektrik termasuk
  yang bukan berkaitan dengan elektrik seperti paip air, rangka rumah dan
  sbgnya.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Cara
  dan Istilah Pembumian</span></i></b><b><i><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">
        </span></i></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><br>
  
  Pembumian dilakukan dengan menyambungkan apa yang perlu dibumikan ke
  dawai atau
  punca bumi pengguna;</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Dawai
  pengikat sama upaya</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Bumi
  - sambungan yang berkesan ke bumi</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Elektrod
  Bumi - Batang (rod) logam, plat logam dan sistem paip
  logam dalam tanah atau sebarang benda berpengalir bagi memperoleh
  sambungan
  bumi yang berkesan</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pengalir
  perlindung litar</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Dawai
  pembumi</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Dawai
  pengikat sama upaya, 10 – Punca bumi pengguna.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 309px; height: 324px;" alt="" src="dist/img/step2/istilah pembumian.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Istilah
  – istilah pembumian</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Elektrod
  Bumi</span></i></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><br>
  
  Berikut adalah jenis elektrod yang diperakui dan memenuhi kehendak
  peraturan
  pendawaian.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Rod
  dan paip – paip bumi</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pita
  atau dawai – dawai bumi</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Plat–plat
  bumi</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Elektrod–elektrod
  bumi yang terbenam di dalam tanah</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Besi
  konkrit yang bertetulang logam</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sistem
  paip logam</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Sarung
  plumbum dan sarung daripada logam yang digunakan untuk
  menyarung kabel dalam tanah</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Struktur
  – struktur bawah tanah lain yang bersesuaian</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"></span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><img style="width: 324px; height: 126px;" alt="" src="dist/img/step2/jenis elektrod bumi.png"><br>
  
        <i style="">Jenis – jenis elektrod bumi</i></span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><img style="width: 324px; height: 177px;" alt="" src="dist/img/step2/jenis jenis elektrod.png"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  Litar Bocor ke Bumi (ELCB RCD RCCB)</span></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar bocor ke bumi digunakan apabila pembumian yang
  baik tidak diperolehi kerana galangan bagi gelung litar rosak ke bumi
  terlalu
  tinggi sehingga tidak mampu untuk memutuskan pelindung arus lebih.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar
  ini boleh bekerja pada voltan yang rendah (50V).</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 270px; height: 324px;" alt="" src="dist/img/step2/pemutus litar.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar bocor ke bumi</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Umumnya
  setiap rumah atau mana – mana pemasangan elektrik akan
  dipasang dengan alat ini. Terdapat beberapa jenis ELCB, antaranya yang
  kini
  digunakan:<span style="font-style: italic;"><span style="font-weight: bold;"></span></span></span></p>
  
        <ul>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar bocor ke bumi kendalian arus baki.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar bocor ke bumi jenis arus imbang.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
          <li><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar bocor ke bumi dengan menggunakan geganti.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></li>
  
        </ul>
  
        <span style="font-family: Symbol;"><span style=""><span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;"></span></span></span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"></span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span>
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><b style=""><i style=""><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  Litar Bocor ke Bumi Kendalian
  Arus Baki</span></i></b><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);"><br>
  
  Alat ini digunakan untuk sistem 1 fasa. Ia amat sensitif, berkendali
  pada nilai
  arus yang kurang daripada 2% daripada arus litar dan dapat mengawal
  pendawaian
  di rumah. Merujuk kepada litarnya, apabila berlaku kebocoran, arus pada
  gelung
  Z dan Y menjadi tidak seimbang, dengan itu gegelung L akan mendapat
  voltan
  aruhan. Keadaan ini menyebabkan gegelung berkendali (beroperasi) kerana
  mendapat tenaga elektrik daripada gegelung L dan seterusnya menarik
  suis pemutus
  litar ke kedudukan off. Arus bocor akan masuk ke bumi melalui pengalir
  pelindung litar bumi jika kebocoran itu berlaku antara
  “live” dan “bumi”.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 315px; height: 311px;" alt="" src="dist/img/step2/pemutus litar kendalian arus.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar ke bumi kendalian arus
  baki</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  Litar Bocor ke Bumi Jenis
  Arus Imbang</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Alat
  ini digunakan pada pemasangan sistem 3 fasa.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Alat
  ini dapat
  berkendali pada arus bocor yang tidak melebihi daripada 2% arus yang
  mengalir.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Apabila
  berlaku kebocoran, keseimbangan arus pada setiap talian 3 fasa itu
  tidak wujud lagi
  dan arus berlebihan di mana–mana bahagian talian tersebut
  akan menyebabkan
  gegelung belantik mendapat voltan aruhan dan arus akan mengalir dalam
  litar.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;">
        </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Seterusnya
  suis pemutus litar akan ditarik ke kedudukan “off”.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 303px;" alt="" src="dist/img/step2/pemutus litar arus imbang.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar bocor ke bumi jenis
  arus imbang</span></i><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></i></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 12pt; line-height: 150%;"><b><i><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  Litar Bocor ke Bumi dengan
  Menggunakan Geganti (Relay)</span></i></b><span style="font-size: 12pt; line-height: 150%; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; margin-bottom: 3pt; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Kaedah
  ini lebih cekap dan litar asasnya seperti rajah.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">R
  adalah
  gegelung geganti yang akan menutup sesentuh apabila voltan gegelung
  mencapai
  satu nilai yang boleh disetkan iaitu semasa berlaku arus bocor ke bumi.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"> </span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Apabila
  berlaku kebocoran arus ke bumi, litar gegelung akan berkendali kerana
  ia akan
  menjadi lengkap, seterusnya ia akan menarik sesentuh utama pemutus
  litar ke
  kedudukan mati (off) dan dengan itu litar bekalan akan terputus.</span><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;;"><o:p></o:p></span><img style="width: 324px; height: 192px;" alt="" src="dist/img/step2/pemutus litar geganti.png"></p>
  
        <p class="MsoNormal" style="background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; line-height: 150%;"><i style=""><span style="font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; color: rgb(51, 51, 51);">Pemutus
  litar ke bumi dengan
  menggunakan geganti</span><o:p></o:p></i></p>
  
        <p class="MsoNormal"><o:p>&nbsp;</o:p></p>
  
        </td>
  
        <td style="text-align: center; vertical-align: top; width: 230px;"><!-- Histats.com (div with counter) -->
        <div id="histats_counter"></div>
  
  <!-- Histats.com START (aync)-->
        <script type="text/javascript">var _Hasync= _Hasync|| [];
  _Hasync.push(['Histats.start', '1,1826156,4,24,200,50,00010001']);
  _Hasync.push(['Histats.fasi', '1']);
  _Hasync.push(['Histats.track_hits', '']);
  (function() {
  var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
  hs.src = ('//s10.histats.com/js15_as.js');
  (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
  })();</script>
        <noscript><a href="/" target="_blank"><img
  src="//sstatic1.histats.com/0.gif?1826156&101" alt="free webpage
  hit counter" border="0"></a></noscript>
  
  <!-- Histats.com END --> <br>
  
        <br>
  
        <script async="" src="dist/img/step2/f.txt"></script><!-- Leftside --><ins class="adsbygoogle" style="display: block;" data-ad-client="ca-pub-3694619358399549" data-ad-slot="3665547928" data-ad-format="auto"></ins>
        <script>
  (adsbygoogle = window.adsbygoogle || []).push({});
        </script><br>
  
        <br>
  
        <br>
  
        <br>
  
        <br>
  
        <br>
  
        <br>
  
        <br>
  
        <br>
  
        <br>
  
        <script async="" src="dist/img/step2/f.txt"></script><!-- Leftside --><ins class="adsbygoogle" style="display: block;" data-ad-client="ca-pub-3694619358399549" data-ad-slot="3665547928" data-ad-format="auto"></ins>
        <script>
  (adsbygoogle = window.adsbygoogle || []).push({});
        </script></td>
  
      </tr>
  
    </tbody>
  </table>
  