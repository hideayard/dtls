<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Digital TVET Academy (DATA)</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <style>
    /* Banner */

	#banner {
		/* background-color: #000000; */
		background-color: rgba(26, 25, 25, 0.97);
		background-image: url("dist/img/banner.jpg");
		background-size: cover;
		background-position: center center;
		background-repeat: no-repeat;
		color: #ffffff;
		padding: 6em 0;
		text-align: center;
		position: relative;
    height: 100%;
	}

		#banner:before {
			content: '';
			position: absolute;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			background: rgba(64, 72, 80, 0.55);
		}

		#banner .inner {
			color: #ffffff;

			position: relative;
			z-index: 1;
		}

			#banner .inner :last-child {
				margin-bottom: 0;
			}

		#banner h2, #banner h3, #banner h4, #banner h5, #banner h6 {
			color: #ffffff;
		}

		#banner .button.alt {
			box-shadow: inset 0 0 0 1px rgba(144, 144, 144, 0.75);
			color: #ffffff !important;
		}

			#banner .button.alt:hover {
				background-color: rgba(144, 144, 144, 0.275);
			}

			#banner .button.alt:active {
				background-color: rgba(144, 144, 144, 0.4);
			}

			#banner .button.alt.icon:before {
				color: #c1c1c1;
			}

		#banner .button {
			min-width: 12em;
		}

		#banner h2 {
			font-size: 3.5em;
			line-height: 1em;
			margin: 0 0 0.5em 0;
			padding: 0;
		}

		#banner p {
			font-size: 1.5em;
			margin-bottom: 2em;
		}

			#banner p a {
				color: #ffffff;
				text-decoration: none;
			}
    </style>
</head>
<body class="hold-transition layout-top-nav layout-navbar-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="index.php" class="navbar-brand">
        <img src="dist/img/tvet_50.png" alt="TVET Logo" class="brand-image"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Digital TVET Academy (DATA)</span>
      </a>
      
     
 <!-- Right navbar links -->
    <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="btn btn-success" href="index.php" class="nav-link">1000 POINTS</a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">KHAIR NOORDIN (LEARNER)</a>
            </li>
           <!-- Messages Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-comments"></i>
                <span class="badge badge-danger navbar-badge">3</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                    <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                    <div class="media-body">
                        <h3 class="dropdown-item-title">
                        Brad Diesel
                        <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                        </h3>
                        <p class="text-sm">Call me whenever you can...</p>
                        <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                    </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                    <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                    <div class="media-body">
                        <h3 class="dropdown-item-title">
                        John Pierce
                        <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                        </h3>
                        <p class="text-sm">I got your message bro</p>
                        <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                    </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                    <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                    <div class="media-body">
                        <h3 class="dropdown-item-title">
                        Nora Silvester
                        <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                        </h3>
                        <p class="text-sm">The subject goes here</p>
                        <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                    </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a id="dropdownAccount" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="far fa-user"></i></a>
                
                <ul aria-labelledby="dropdownAccount" class="dropdown-menu border-0 shadow">
                  <li> <a href="index.php" class="dropdown-item">Sign Out</a></li>
                </ul>
            </li>
            </ul>
            <!-- SEARCH FORM -->
            <form class="form-inline ml-0 ml-md-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                </button>
                </div>
            </div>
            </form>
        </div>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->
<div class="row">
    <div class="col-lg-12">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Domestic Wiring</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Course Detail</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
            <div class="container-fluid">
           
                <!-- Main row -->
                <div class="row">
                <!-- Left col -->
                <div class="col-md-12">
                <div class="card">
              <div class="card-header bg-info">
                <h3 class="card-title">Domestic Wiring (<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-secondary"></i>)</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                        <div class="col-4" style="text-align:center;">
                            <img class="img-fluid pad" src="dist/img/wiring_600.jpg" alt="Photo">
                            <br>
                            <br>
                            <button onclick="startcourse();" class="btn btn-info">Start Course</button>
                        </div>
                        <div class="col-8">
                            <div class="callout callout-info">
                                <h5>Description</h5>
                                <p>This course will give you and understanding of:
                                    <ul>
                                        <li>Single phase drawing</li>
                                        <li>Single phase wiring</li>
                                        <li>Single phase wiring maintenance</li>
                                        <li>Single phase motor</li>
                                        <li>Single phase electrical appliances maintenance</li>
                                    </ul>
                                </p>    
                            </div>
                            <div class="callout callout-warning">
                                <h5>Content</h5>
                                <ul>
                                    <li>Course Introduction (Video)</li>
                                    <li>Wiring drawing for single phase (web content)</li>
                                    <li>Tutorial video on drawing (Video)</li>
                                    <li>Handling electrical equipment (Powerpoint)</li>
                                    <li>Tools in electrical wiring (Video and Interactive content)</li>
                                    <li>Hands on wiring (interactive game)</li>
                                    <li>Domestic Wiring (test)</li>
                                    <li>Exit course survey(survey)</li>
                                </ul>
                            </div>

                            <div class="callout callout-success">
                                <h5>Completion Rule</h5>
                                <ul>
                                    <li>You must complete  all the units in this course including the test and the survey</li>
                                    <li>Leads to a certification with a duration : Forever</li>

                                </ul>
                            </div>
                
                        </div>
                    </div>
                </div>
    
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                
            </div>
                   
                </div>
                <!-- /.col -->

                <div class="col-md-4">
                    

                   
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
  </div>
</div>


  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Web Developer
    </div>
    <!-- Default to the left -->
    <strong>Developed &copy;2020 by <a href="https://microalvine.com">M4D Studio</a>.</strong>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<script>
function startcourse()
{
   
            Swal.fire({
                title: 'Are you sure to Start This Course?',
                text: "You won't be able to revert this!",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Start Now!'
                }).then((result) => {
                    console.log(result);
                if (result.isConfirmed || result.value) {
                    Swal.fire(
                    'Enrolled!',
                    'You have Started to Enroll this Course.',
                    'success'
                    );
                    setTimeout(function(){ window.location='course_start.php'; }, 2000);

                }
                });
}
</script>
</body>
</html>
